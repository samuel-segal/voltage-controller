//Type definitions
typedef int pin;
typedef int joystick;
typedef float voltage;

//Pin constants
const pin buttonJoystickPin = A0;
const pin xJoystickPin = A1;
const pin yJoystickPin = A2;

const pin xVoltagePin = 3;
const pin yVoltagePin = 5;

//General constants
const int dt = 50;
const int neutralValue = 512;
const float joystickConversionConstant = 1e-3;
const joystick noiseThreshold = 10;
const voltage minVoltage = 0;
const voltage maxVoltage = 255;

//Voltage of pins
voltage xVoltage = 0;
voltage yVoltage = 0;


void setup() {
  pinMode(buttonJoystickPin, INPUT);
  pinMode(xJoystickPin, INPUT);
  pinMode(yJoystickPin, INPUT);

  pinMode(xVoltagePin, OUTPUT);
  pinMode(yVoltagePin, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  int button = digitalRead(buttonJoystickPin);
  if (button==LOW){
    xVoltage = 0;
    yVoltage = 0;
  }

  int x = analogRead(xJoystickPin);
  int y = analogRead(yJoystickPin);

  //Converts the analog readings into usable vectors
  joystick jx = toJoystickVector(x);
  joystick jy = toJoystickVector(y);

  //Eliminates the possiblity that measurement errors affect voltage
  normalizeJoystickVector(&jx);
  normalizeJoystickVector(&jy);

  //Controlled for smaller sampling sizes
  voltage dvx = toVoltage(jx) * dt;
  voltage dvy = toVoltage(jy) * dt;

  //Adds voltages from joystick
  xVoltage += dvx;
  yVoltage += dvy;

  //Clamps the voltages
  clampVoltage(&xVoltage);
  clampVoltage(&yVoltage);

  int analogX = toAnalog(xVoltage);
  int analogY = toAnalog(yVoltage);
  
  analogWrite(xVoltagePin, analogX);
  analogWrite(yVoltagePin, analogY);

  printVoltages();
  
  delay(dt);
}


//Subtracts the base value from the reading, giving a one dimensional vector in the joystick type
joystick toJoystickVector(int reading){
  return reading - neutralValue;
}

//If the value is not firmly out of the margin of error, it is normalized to 0
void normalizeJoystickVector(joystick * j){
  *j = abs(*j) > noiseThreshold ? *j : 0;
}

//Converts the joystick reading vector to the signified voltage
voltage toVoltage(joystick j){
  return j * joystickConversionConstant;
}

//Ensures that voltage is not higher than 255, or lower than 0
void clampVoltage(voltage * t){
  *t = max(min(*t,maxVoltage),minVoltage);
}

int toAnalog(voltage v){
  return (int)v;
}

void printVoltages(){
  Serial.print(xVoltage);
  Serial.print('\t');
  Serial.println(yVoltage);
}
